package co.matcha.backendcase

import org.springframework.data.domain.Page
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/users/scores")
class UserScoreApi {
    @GetMapping
    fun getUserScores(
        // The 'query' should match the 'firstName' or 'lastName'
        // If you want to go the extra mile, you can use ngram and fuzzy search
        @RequestParam query: String,

        // The 'orderBy' parameter allows sorting results using the following format 'column:order'
        // e.g. 'firstName:DESC'
        @RequestParam(required = false) orderBy: String? = null,

        page: Int? = 0,
        size: Int = 20,
    ): Page<UserScoreResource> {
        //TODO: Return the page of results based on the query
        return Page.empty()
    }
}

data class UserScoreResource(
    val firstName: String,
    val lastName: String,
    val score: Double,
    val confidence: Double,
)
