package co.matcha.backendcase

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BackendCaseApplication

fun main(args: Array<String>) {
    runApplication<BackendCaseApplication>(*args)
}
