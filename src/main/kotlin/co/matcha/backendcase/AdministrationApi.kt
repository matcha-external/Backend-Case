package co.matcha.backendcase

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/administration")
class AdministrationApi {
    @GetMapping
    fun reindexData(): ResponseEntity<Void> {
        // TODO: Trigger an asynchronous re-indexation of the data into ES
        return ResponseEntity.accepted().build()
    }
}
