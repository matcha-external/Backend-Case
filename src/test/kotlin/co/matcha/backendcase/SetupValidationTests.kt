package co.matcha.backendcase

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.cloud.bigquery.BigQuery
import com.google.cloud.bigquery.QueryJobConfiguration
import org.assertj.core.api.Assertions.assertThat
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.core.CountRequest
import org.elasticsearch.client.indices.GetIndexRequest
import org.elasticsearch.cluster.health.ClusterHealthStatus
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.MatchAllQueryBuilder
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class SetupValidationTests {
    @Autowired
    private lateinit var bigquery: BigQuery

    @Autowired
    private lateinit var esClient: RestHighLevelClient

    @Test
    fun `ensure Elastic Search connection works properly`() {
        val healthStatus = esClient.cluster().health(
            ClusterHealthRequest(),
            RequestOptions.DEFAULT
        )

        assertThat(healthStatus.status).isNotEqualTo(ClusterHealthStatus.RED)
    }

    @Test
    fun `ensure that ES indexation works properly`() {
        data class TestDocument(
            val firstName: String,
            val lastName: String
        )

        val indexName = "test-user"

        val document = TestDocument("Maxime", "Gaudin")

        givenAnEmptyIndex(indexName)
        givenAnIndexedDocument(indexName, document)

        // When
        val searchResult = esClient.search(
            SearchRequest(indexName)
                .source(SearchSourceBuilder().query(MatchAllQueryBuilder())),
            RequestOptions.DEFAULT
        )
        val matchingDocuments = searchResult.hits.map {
            jacksonObjectMapper().readValue(it.sourceAsString, TestDocument::class.java)
        }

        // Then
        assertThat(matchingDocuments).hasSize(1)
        assertThat(matchingDocuments.first()).isEqualTo(document)
    }

    private fun givenAnIndexedDocument(
        indexName: String,
        document: Any
    ) {
        esClient.index(
            IndexRequest(indexName)
                .source(
                    jacksonObjectMapper().writeValueAsString(document),
                    XContentType.JSON
                ),
            RequestOptions.DEFAULT
        )

        waitForAtLeastOneDocumentIsIndexed(indexName)
    }

    private fun givenAnEmptyIndex(indexName: String) {
        val alreadyExists = esClient.indices().exists(
            GetIndexRequest(indexName),
            RequestOptions.DEFAULT
        )
        if (alreadyExists) {
            esClient.indices().delete(
                DeleteIndexRequest(indexName),
                RequestOptions.DEFAULT
            )
        }
    }

    @Test
    fun `ensure BigQuery connection works properly`() {
        val query = "SELECT * FROM data_technical_case.contacts"
        val queryResult = bigquery.query(QueryJobConfiguration.newBuilder(query).build())

        assertThat(queryResult.totalRows).isGreaterThan(0)
    }

    private fun waitForAtLeastOneDocumentIsIndexed(indexName: String) {
        do {
            Thread.sleep(100)
        } while (esClient.count(CountRequest(indexName), RequestOptions.DEFAULT).count == 0L)
    }
}