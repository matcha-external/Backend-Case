# Matcha: Backend Case

## Introduction

Wow! If you read this, it's because we clicked during the discovery interview and we want to assess your technical
abilities! Congratulation <3

In essence, Matcha's job is to compute a score for every user. To allow you to show your technical skills in a sensible
context, this case will "replicate" one use-case here at Matcha:  Index the users's scores and expose it via a REST API.

The case is written in Kotlin, however you can write your code in Java if you are more comfortable with it.

The amount of time you spend on this case is up to you. The goal is to demonstrate your abilities and habits of coding,
architecture, testing, etc. You can go as far as you want.

**Keep in mind that this test contains voluntary approximations, mistake even. Write your code as you would usually and feel free (obligated?) to change anything you consider bad practices or possible optimization/improvements**

We will use the code you wrote as a support for the technical review with the whole team.

# How?

## Run the tests

To ensure you have everything to work properly, we wrote a test called `SetupValidationTest`. This test must run
properly before starting the case. If you cannot figure out what's wrong, please email me at `maxime@matcha.co` with the
problem report.

**Make sure to read them thoroughly, they contain very useful information about**

- How to query BigQuery
- How to create an ES Index
- How to index a document
- How to query a document

## Ensure your local environment is ready

### Docker (ElasticSearch)

To run a local ElasticSearch instance, just use the docker-compose.yml file available in `src/main/docker`.

### BigQuery Connection

To be able to connect to our test dataset, you must:

1. Install the `glcoud` CLI: https://cloud.google.com/sdk/docs/install
2. Login using your **personal gmail account** (I will grant you the right when I send you the
   case): `gcloud auth application-default login`
3. Spring boot will detect that you are logged in and will use these credentials

**Please note that in this case, BigQuery will behave exactly like a SQL database. Not BigQuery magic is required.**.

#### Additional information

*Project ID*: shake-eu-staging

*Dataset ID*: data_technical_case

*Tables*:

- contacts: This table contains the list of users. This is the table that contains the `firstName` and `lastName`
  information
- individual_score_states: This table contains the score computed for every user. This is the table that contains
  the `score` and `confidence` value. (the join key with the `contacts` table to use is `contact_id_ref`)

The information related to the driver is available
here: https://docs.spring.io/spring-cloud-gcp/docs/current/reference/html/bigquery.html

## Fill in the blank

Now that you are all set, you have two API to implement:
- UserScoreApi: Query the indexed data to expose them to the API
- AdministrationAPI: Get data from BigQuery &  Index this data inside ES


